package weatherapp.codetest.com.weatherapp.core.network.services.weather;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import weatherapp.codetest.com.weatherapp.core.network.services.weather.response.WeatherDetails;

/**
 * Created by M1019318 on 11/15/2017.
 */

public interface GetCityWeatherReport {
    @GET("data/2.5/forecast")
    Call<WeatherDetails> getCityWeather(@QueryMap Map<String, String> options);
}
