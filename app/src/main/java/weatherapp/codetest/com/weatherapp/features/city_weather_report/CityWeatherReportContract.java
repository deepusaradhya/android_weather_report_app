package weatherapp.codetest.com.weatherapp.features.city_weather_report;

import weatherapp.codetest.com.weatherapp.core.network.services.weather.response.WeatherDetails;

/**
 * Created by M1019318 on 2/2/2018.
 */

public interface CityWeatherReportContract {
    interface View{

        void displayTitle(String title);

        void initiateList(WeatherDetails weatherDetails);

        void showError();
    }

    interface Presenter {
        void getCityWeatherReport(String city);
    }
}
