package weatherapp.codetest.com.weatherapp.features.city_weather_report.view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import weatherapp.codetest.com.weatherapp.R;

/**
 * Created by M1019318 on 2/1/2018.
 */

public class WeatherListItemViewHolder extends RecyclerView.ViewHolder {

    public TextView mDate;
    public TextView mMinTemp;
    public TextView mMaxTemp;

    public WeatherListItemViewHolder(View itemView) {
        super(itemView);

        mDate = itemView.findViewById(R.id.date_text_view);
        mMinTemp = itemView.findViewById(R.id.min_temp_text_view);
        mMaxTemp = itemView.findViewById(R.id.max_temp_text_view);
    }
}
