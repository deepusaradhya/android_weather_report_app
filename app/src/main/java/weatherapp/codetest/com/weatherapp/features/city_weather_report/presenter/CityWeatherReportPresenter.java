package weatherapp.codetest.com.weatherapp.features.city_weather_report.presenter;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import weatherapp.codetest.com.weatherapp.core.network.ApiClient;
import weatherapp.codetest.com.weatherapp.core.network.services.weather.GetCityWeatherReport;
import weatherapp.codetest.com.weatherapp.core.network.services.weather.response.WeatherDetails;
import weatherapp.codetest.com.weatherapp.features.city_weather_report.CityWeatherReportContract;
import weatherapp.codetest.com.weatherapp.features.city_weather_report.CityWeatherReportContract.View;

/**
 * Created by M1019318 on 2/2/2018.
 */

public class CityWeatherReportPresenter implements CityWeatherReportContract.Presenter {

    private CityWeatherReportContract.View mView;

    public CityWeatherReportPresenter(@NonNull View view) {
        mView = view;
    }

    @Override
    public void getCityWeatherReport(String city) {
        GetCityWeatherReport mGetCityWeatherReport = ApiClient.getClient().create(GetCityWeatherReport.class);
        Map<String, String> data = new HashMap<>();
        data.put("appid", "daedf5f06bf6c41a48b1821c79ff1bd2");
        data.put("units", "metric");
        data.put("q", city);
        Call<WeatherDetails> call2 = mGetCityWeatherReport.getCityWeather(data);
        call2.enqueue(new Callback<WeatherDetails>() {
            @Override
            public void onResponse(Call<WeatherDetails> call, Response<WeatherDetails> response) {
                if (response.body() != null) {
                    handleSuccessResponse(response.body());
                    mView.initiateList(response.body());
                }
                else {
                    mView.showError();
                }

            }

            @Override
            public void onFailure(Call<WeatherDetails> call, Throwable t) {
                mView.showError();
            }
        });
    }

    public void handleSuccessResponse(WeatherDetails weatherDetails) {
        StringBuilder cityName = new StringBuilder();
        if (weatherDetails != null && weatherDetails.getCity() != null && weatherDetails.getCity().getName() != null && weatherDetails.getCity().getCountry() != null) {
            cityName.append(weatherDetails.getCity().getName());
            cityName.append(" , ");
            cityName.append(weatherDetails.getCity().getCountry());
            mView.displayTitle(cityName.toString());
        }
    }
}
