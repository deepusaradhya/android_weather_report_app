package weatherapp.codetest.com.weatherapp.features.search.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import weatherapp.codetest.com.weatherapp.R;
import weatherapp.codetest.com.weatherapp.core.config.AppConstants;
import weatherapp.codetest.com.weatherapp.features.city_weather_report.view.CityWeatherListActivity;

public class SearchCityWeatherActivity extends AppCompatActivity implements OnClickListener {

    Button mSearchButton;
    TextInputLayout mSearchTextInputLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_city_weather);
        mSearchButton = findViewById(R.id.search_button);
        mSearchButton.setOnClickListener(this);
        mSearchTextInputLayout = findViewById(R.id.search_input_layout);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.search_button) {
            if (mSearchTextInputLayout.getEditText().getText().toString().equals("")) {
                Toast.makeText(SearchCityWeatherActivity.this, getString(R.string.enter_search_text), Toast.LENGTH_LONG).show();;
            }
            else {
                Intent intent = new Intent(SearchCityWeatherActivity.this, CityWeatherListActivity.class);
                intent.putExtra(AppConstants.SEARCH_STRING, mSearchTextInputLayout.getEditText().getText().toString());
                startActivity(intent);
            }
        }
    }
}
