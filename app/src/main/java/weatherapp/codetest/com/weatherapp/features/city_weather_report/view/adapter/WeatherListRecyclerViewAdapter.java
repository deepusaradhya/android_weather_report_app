package weatherapp.codetest.com.weatherapp.features.city_weather_report.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import weatherapp.codetest.com.weatherapp.R;
import weatherapp.codetest.com.weatherapp.features.city_weather_report.view.viewholder.WeatherListItemViewHolder;

/**
 * Created by M1019318 on 2/1/2018.
 */

public class WeatherListRecyclerViewAdapter extends RecyclerView.Adapter<WeatherListItemViewHolder> {

    List<weatherapp.codetest.com.weatherapp.core.network.services.weather.response.List> mCityWeatherList;
    Context mContext;
    View mView;
    WeatherListItemViewHolder mViewHolder;

    public WeatherListRecyclerViewAdapter(Context context, List<weatherapp.codetest.com.weatherapp.core.network.services.weather.response.List> cityWeatherList) {

        mCityWeatherList = cityWeatherList;
        mContext = context;
    }

    @Override
    public WeatherListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mView = LayoutInflater.from(mContext).inflate(R.layout.list_row_item, parent, false);
        mViewHolder = new WeatherListItemViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(WeatherListItemViewHolder holder, int position) {

        holder.mDate.setText(mContext.getText(R.string.date) + mCityWeatherList.get(position).getDtTxt());
        holder.mMinTemp.setText(mContext.getText(R.string.min) + String.valueOf(mCityWeatherList.get(position).getMain().getTempMin()));
        holder.mMaxTemp.setText(mContext.getText(R.string.max) + String.valueOf(mCityWeatherList.get(position).getMain().getTempMax()));

    }

    @Override
    public int getItemCount() {
        return mCityWeatherList.size();
    }
}
