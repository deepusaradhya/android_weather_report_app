package weatherapp.codetest.com.weatherapp.features.city_weather_report.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import weatherapp.codetest.com.weatherapp.R;
import weatherapp.codetest.com.weatherapp.core.config.AppConstants;
import weatherapp.codetest.com.weatherapp.core.network.services.weather.response.WeatherDetails;
import weatherapp.codetest.com.weatherapp.features.city_weather_report.CityWeatherReportContract;
import weatherapp.codetest.com.weatherapp.features.city_weather_report.presenter.CityWeatherReportPresenter;
import weatherapp.codetest.com.weatherapp.features.city_weather_report.view.adapter.WeatherListRecyclerViewAdapter;

public class CityWeatherListActivity extends AppCompatActivity implements CityWeatherReportContract.View {

    private RecyclerView mCityWeatherRecyclerView;
    private RecyclerView.LayoutManager recyclerViewLayoutManager;
    private WeatherListRecyclerViewAdapter mWeatherListRecyclerViewAdapter;
    private String mCity;
    private CityWeatherReportPresenter mPresenter;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_weather_list);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra(AppConstants.SEARCH_STRING) != null) {
                mCity = intent.getStringExtra(AppConstants.SEARCH_STRING);
            }
        }

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getString(R.string.progress_bar_updating_text));
        mProgressDialog.show();

        mPresenter = new CityWeatherReportPresenter(this);
        mCityWeatherRecyclerView = findViewById(R.id.city_weather_recycler_view);
        mPresenter.getCityWeatherReport(mCity);

    }

    @Override
    public void displayTitle(String title) {
        mProgressDialog.dismiss();
        setTitle(title);
    }

    @Override
    public void initiateList(WeatherDetails weatherDetails) {
        recyclerViewLayoutManager = new LinearLayoutManager(this);
        mCityWeatherRecyclerView.setLayoutManager(recyclerViewLayoutManager);
        mWeatherListRecyclerViewAdapter = new WeatherListRecyclerViewAdapter(this, weatherDetails.getList());
        mCityWeatherRecyclerView.setAdapter(mWeatherListRecyclerViewAdapter);
    }

    @Override
    public void showError() {
        mProgressDialog.dismiss();
        Toast.makeText(this, getText(R.string.network_error_msg), Toast.LENGTH_LONG).show();
    }
}
