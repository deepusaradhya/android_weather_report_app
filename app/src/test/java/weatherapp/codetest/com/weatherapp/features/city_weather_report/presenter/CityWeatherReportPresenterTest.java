package weatherapp.codetest.com.weatherapp.features.city_weather_report.presenter;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import weatherapp.codetest.com.weatherapp.core.network.services.weather.response.City;
import weatherapp.codetest.com.weatherapp.core.network.services.weather.response.WeatherDetails;
import weatherapp.codetest.com.weatherapp.features.city_weather_report.view.CityWeatherListActivity;

/**
 * Created by M1019318 on 2/6/2018.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({CityWeatherReportPresenter.class})
public class CityWeatherReportPresenterTest {

    private CityWeatherListActivity mockView;
    private CityWeatherReportPresenter mockCityWeatherReportPresenter;
    private boolean isCallBackTriggered;
    private final static int TIMEOUT = 2000;

    @Before
    public void setUp() {
        // mock necessary objects
        mockView = PowerMockito.mock(CityWeatherListActivity.class);
        mockCityWeatherReportPresenter = PowerMockito.mock(CityWeatherReportPresenter.class);
        Whitebox.setInternalState(mockCityWeatherReportPresenter, "mView", mockView);
        isCallBackTriggered = false;
    }

    /*StringBuilder cityName = new StringBuilder();
        if (weatherDetails != null && weatherDetails.getCity().getName() != null && weatherDetails.getCity().getCountry() != null) {
        cityName.append(weatherDetails.getCity().getName());
        cityName.append(" , ");
        cityName.append(weatherDetails.getCity().getCountry());
        mView.displayTitle(cityName.toString());
    }*/

    @Test
    public void testHandleSuccessResponseForNullWeatherDetailsReport() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        WeatherDetails mockWeatherDetails = null;
        PowerMockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                latch.countDown();
                isCallBackTriggered = true;
                return null;
            }
        }).when(mockView).displayTitle(Mockito.anyString());
        Mockito.doCallRealMethod().when(mockCityWeatherReportPresenter).handleSuccessResponse(Mockito.any(WeatherDetails.class));
        mockCityWeatherReportPresenter.handleSuccessResponse(mockWeatherDetails);
        latch.await(TIMEOUT, TimeUnit.MILLISECONDS);
        Assert.assertFalse("Callback is triggered", isCallBackTriggered);

    }

    @Test
    public void testHandleSuccessResponseForNotNullWeatherDetailsReport() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        WeatherDetails mockWeatherDetails = Mockito.mock(WeatherDetails.class);
        PowerMockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                latch.countDown();
                isCallBackTriggered = true;
                return null;
            }
        }).when(mockView).displayTitle(Mockito.anyString());
        Mockito.doCallRealMethod().when(mockCityWeatherReportPresenter).handleSuccessResponse(Mockito.any(WeatherDetails.class));
        mockCityWeatherReportPresenter.handleSuccessResponse(mockWeatherDetails);
        latch.await(TIMEOUT, TimeUnit.MILLISECONDS);
        Assert.assertFalse("Callback is triggered", isCallBackTriggered);

    }

    @Test
    public void testHandleSuccessResponseForNullCity() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        WeatherDetails mockWeatherDetails = Mockito.mock(WeatherDetails.class);
        City mockCity = null;
        PowerMockito.doReturn(mockCity).when(mockWeatherDetails).getCity();
        PowerMockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                latch.countDown();
                isCallBackTriggered = true;
                return null;
            }
        }).when(mockView).displayTitle(Mockito.anyString());
        Mockito.doCallRealMethod().when(mockCityWeatherReportPresenter).handleSuccessResponse(Mockito.any(WeatherDetails.class));
        mockCityWeatherReportPresenter.handleSuccessResponse(mockWeatherDetails);
        latch.await(TIMEOUT, TimeUnit.MILLISECONDS);
        Assert.assertFalse("Callback is triggered", isCallBackTriggered);

    }

    @Test
    public void testHandleSuccessResponseForNotNullCity() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        WeatherDetails mockWeatherDetails = Mockito.mock(WeatherDetails.class);
        City mockCity = Mockito.mock(City.class);
        PowerMockito.doReturn(mockCity).when(mockWeatherDetails).getCity();
        PowerMockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                latch.countDown();
                isCallBackTriggered = true;
                return null;
            }
        }).when(mockView).displayTitle(Mockito.anyString());
        Mockito.doCallRealMethod().when(mockCityWeatherReportPresenter).handleSuccessResponse(Mockito.any(WeatherDetails.class));
        mockCityWeatherReportPresenter.handleSuccessResponse(mockWeatherDetails);
        latch.await(TIMEOUT, TimeUnit.MILLISECONDS);
        Assert.assertFalse("Callback is triggered", isCallBackTriggered);

    }

    @Test
    public void testHandleSuccessResponseForNullName() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        WeatherDetails mockWeatherDetails = Mockito.mock(WeatherDetails.class);
        City mockCity = Mockito.mock(City.class);
        String mockName = null;
        PowerMockito.doReturn(mockCity).when(mockWeatherDetails).getCity();
        PowerMockito.doReturn(mockName).when(mockCity).getName();
        PowerMockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                latch.countDown();
                isCallBackTriggered = true;
                return null;
            }
        }).when(mockView).displayTitle(Mockito.anyString());
        Mockito.doCallRealMethod().when(mockCityWeatherReportPresenter).handleSuccessResponse(Mockito.any(WeatherDetails.class));
        mockCityWeatherReportPresenter.handleSuccessResponse(mockWeatherDetails);
        latch.await(TIMEOUT, TimeUnit.MILLISECONDS);
        Assert.assertFalse("Callback is triggered", isCallBackTriggered);

    }

    @Test
    public void testHandleSuccessResponseForNotNullName() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        WeatherDetails mockWeatherDetails = Mockito.mock(WeatherDetails.class);
        City mockCity = Mockito.mock(City.class);
        String mockName = "Bangalore";
        PowerMockito.doReturn(mockCity).when(mockWeatherDetails).getCity();
        PowerMockito.doReturn(mockName).when(mockCity).getName();
        PowerMockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                latch.countDown();
                isCallBackTriggered = true;
                return null;
            }
        }).when(mockView).displayTitle(Mockito.anyString());
        Mockito.doCallRealMethod().when(mockCityWeatherReportPresenter).handleSuccessResponse(Mockito.any(WeatherDetails.class));
        mockCityWeatherReportPresenter.handleSuccessResponse(mockWeatherDetails);
        latch.await(TIMEOUT, TimeUnit.MILLISECONDS);
        Assert.assertFalse("Callback is triggered", isCallBackTriggered);

    }

    @Test
    public void testHandleSuccessResponseForNullCountry() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        WeatherDetails mockWeatherDetails = Mockito.mock(WeatherDetails.class);
        City mockCity = Mockito.mock(City.class);
        String mockCountry = null;
        PowerMockito.doReturn(mockCity).when(mockWeatherDetails).getCity();
        PowerMockito.doReturn(mockCountry).when(mockCity).getCountry();
        PowerMockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                latch.countDown();
                isCallBackTriggered = true;
                return null;
            }
        }).when(mockView).displayTitle(Mockito.anyString());
        Mockito.doCallRealMethod().when(mockCityWeatherReportPresenter).handleSuccessResponse(Mockito.any(WeatherDetails.class));
        mockCityWeatherReportPresenter.handleSuccessResponse(mockWeatherDetails);
        latch.await(TIMEOUT, TimeUnit.MILLISECONDS);
        Assert.assertFalse("Callback is triggered", isCallBackTriggered);

    }

    @Test
    public void testHandleSuccessResponseForNotNullCountry() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        WeatherDetails mockWeatherDetails = Mockito.mock(WeatherDetails.class);
        City mockCity = Mockito.mock(City.class);
        String mockCountry = "India";
        PowerMockito.doReturn(mockCity).when(mockWeatherDetails).getCity();
        PowerMockito.doReturn(mockCountry).when(mockCity).getCountry();
        PowerMockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                latch.countDown();
                isCallBackTriggered = true;
                return null;
            }
        }).when(mockView).displayTitle(Mockito.anyString());
        Mockito.doCallRealMethod().when(mockCityWeatherReportPresenter).handleSuccessResponse(Mockito.any(WeatherDetails.class));
        mockCityWeatherReportPresenter.handleSuccessResponse(mockWeatherDetails);
        latch.await(TIMEOUT, TimeUnit.MILLISECONDS);
        Assert.assertFalse("Callback is triggered", isCallBackTriggered);
    }

    @Test
    public void testHandleSuccessResponseHappyFlow() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        WeatherDetails mockWeatherDetails = Mockito.mock(WeatherDetails.class);
        City mockCity = Mockito.mock(City.class);
        String mockName = "Bangalore";
        String mockCountry = "India";
        PowerMockito.doReturn(mockCity).when(mockWeatherDetails).getCity();
        PowerMockito.doReturn(mockName).when(mockCity).getName();
        PowerMockito.doReturn(mockCountry).when(mockCity).getCountry();
        PowerMockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                latch.countDown();
                isCallBackTriggered = true;
                return null;
            }
        }).when(mockView).displayTitle(Mockito.anyString());
        Mockito.doCallRealMethod().when(mockCityWeatherReportPresenter).handleSuccessResponse(Mockito.any(WeatherDetails.class));
        mockCityWeatherReportPresenter.handleSuccessResponse(mockWeatherDetails);
        latch.await(TIMEOUT, TimeUnit.MILLISECONDS);
        Assert.assertTrue("Callback is not triggered", isCallBackTriggered);
    }

}
